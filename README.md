# Deploying Concrete5 on Docker

- Clone the repo to your project root.
- Download Concrete5 to the project root.
- Unzip Concrete5 source to the project root.
- and rename the Concrete5 sourcee to C5.

## Running the C5 Docker Stack

If Docker is installed, deploy the stack as a docker-compose env.

    docker-compose up

To run the the stack in the background.

    docker-compose up -d

To rebuild the stack.

    docker-compose up --build

To bring stack down

    docker-compose down

## Customizing the DB

The repo has a env file so you customize your database credentials.

Default env .

    MYSQL_ROOT_PASSWORD=C5RootPassword

    MYSQL_DATABASE=C5

    MYSQL_USER=C5User

    MYSQL_PASSWORD=C5DBPassword

    MYSQL_HOST=mariadb

## The Stack

**Mysql**: mariadb:10.3.17

**PHP**: 7.2.1

**Apache**: 2

## Download C5

    wget --content-disposition <C5 download link>
